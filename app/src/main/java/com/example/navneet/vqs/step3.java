package com.example.navneet.vqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class step3 extends AppCompatActivity {

    Button step3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step3);

        step3= (Button)findViewById(R.id.save3);


        step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 =new Intent(getApplicationContext(), step4.class);
                startActivity(intent4);


            }
        });

    }

}
