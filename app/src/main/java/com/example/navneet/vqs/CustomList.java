package com.example.navneet.vqs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by pc6 on 12-05-2016.
 */

public class CustomList extends ArrayAdapter<String> {
    private Integer[] icon;
    private String[] title;
    private Integer[] camera;
    private String[] title2;
    private String[] title3;
    private String[] title4;
    private String[] title5;



    private Activity context;

    public CustomList(Activity context, Integer[] icon,String[] title, Integer[] camera, String[] title2,String[] title3,String[] title4,String[] title5) {
        super(context, R.layout.row, title);
        this.context = context;
        this.icon = icon;
        this.title = title;
        this.camera = camera;
        this.title2=title2;
        this.title3=title3;
        this.title4=title4;
        this.title5=title5;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row, null, true);
        ImageView icon1 = (ImageView) listViewItem.findViewById(R.id.note);
        TextView title1 = (TextView) listViewItem.findViewById(R.id.quote);
        ImageView camera1 = (ImageView) listViewItem.findViewById(R.id.next);



        icon1.setImageResource(icon[position]);
        title1.setText(title[position]);
        camera1.setImageResource(camera[position]);
        return  listViewItem;
    }
}