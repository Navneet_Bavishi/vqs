package com.example.navneet.vqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class step4_2 extends AppCompatActivity {

    Button step42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4_2);

        step42= (Button)findViewById(R.id.save42);


        step42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent7 =new Intent(getApplicationContext(), step5.class);
                startActivity(intent7);
            }
        });

    }



}
