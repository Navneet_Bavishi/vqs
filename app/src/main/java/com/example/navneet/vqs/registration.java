package com.example.navneet.vqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class registration extends AppCompatActivity {

        Button submit;
        EditText remail;
        EditText rpassword;
        EditText name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        submit=(Button)findViewById(R.id.submit);
        remail=(EditText)findViewById(R.id.remail);
        rpassword=(EditText)findViewById(R.id.rpassword);
        name=(EditText)findViewById(R.id.name);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(remail.getText().toString().trim().length()==0){
                    remail.setError("Username is not entered");
                    remail.requestFocus();
                }

                else if(rpassword.getText().toString().trim().length()==0){
                    rpassword.setError("Password is not entered");
                    rpassword.requestFocus();
                }


                else if(name.getText().toString().trim().length()==0){
                    rpassword.setError("Name is not entered");
                    rpassword.requestFocus();
                }
                else{
                    Intent it=new Intent(getApplicationContext(),Second.class);
                    startActivity(it);
                }
            }


        });

    }
}
