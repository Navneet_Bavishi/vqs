package com.example.navneet.vqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class step4 extends AppCompatActivity {

    Button step4;
    Button step41;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4);

        step4= (Button) findViewById(R.id.save4);
        step41 = (Button) findViewById(R.id.save41);



        step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent5 = new Intent(getApplicationContext(), step4_2.class);
                startActivity(intent5);

            }
        });

        step41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent6 = new Intent(getApplicationContext(),last.class);
                startActivity(intent6);
            }
        });
    }
}
