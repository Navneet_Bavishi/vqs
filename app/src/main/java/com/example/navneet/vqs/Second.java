package com.example.navneet.vqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Second extends AppCompatActivity {
    Button register;
    Button login;
    EditText email;
    EditText password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        register= (Button)findViewById(R.id.register);
        login=(Button)findViewById(R.id.login);
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(email.getText().toString().trim().length()==0){
                        email.setError("Username is not entered");
                        email.requestFocus();
                    }

                    else if(password.getText().toString().trim().length()==0){
                        password.setError("Password is not entered");
                        password.requestFocus();
                    }
                    else{
                        Intent it=new Intent(Second.this,step1.class);
                        startActivity(it);
                    }
                }


            });

            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent inti=new Intent(getApplicationContext(),registration.class);
                    startActivity(inti);
                }
            });





    }



}