package com.example.navneet.vqs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class step5 extends AppCompatActivity {

    Button step5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step5);
        step5= (Button)findViewById(R.id.save5);


        step5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent8 =new Intent(getApplicationContext(), step6.class);
                startActivity(intent8);
            }
        });

    }



}

